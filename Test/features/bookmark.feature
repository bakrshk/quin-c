Feature: Working With Bookmarks

@M
 Scenario Outline: Create Bookmark Context
  Given I am on QuinC page
  When Login to QuinC with "<credentials>"
  Then user select case "<caseInfo>"
  When i create "<bookmarkInfo>" Context
  When I go to Grid
  When i Select Records from grid
  When i Assign "<bookmarkInfo>" from grid
  Then i Select Filter
  When i filter "<bookmarkInfo>"
  Then validate filter "<bookmarkInfo>"
    Examples:
      | QuinC                                        | credentials  | caseInfo | bookmarkInfo |
      | http://lakshmi:4443/LoginPage.html           | admin        | case1002  | test        |









