import {browser, element, by, protractor} from "protractor";
import {el} from "@angular/platform-browser/testing/browser_util";

const chai = require("chai").use(require("chai-as-promised"));
const yaml = require("js-yaml");
const fs = require("fs");
let caseData = yaml.safeLoad(fs.readFileSync("../testData/case.yml", "utf8"));

const expect = chai.expect;

