"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var chai = require("chai").use(require("chai-as-promised"));
var yaml = require("js-yaml");
var fs = require("fs");
var caseData = yaml.safeLoad(fs.readFileSync("../testData/case.yml", "utf8"));
var bookData = yaml.safeLoad(fs.readFileSync('../testData/book.yml', 'utf8'));
var expect = chai.expect;
var bookmarkPage = /** @class */ (function () {
    function bookmarkPage() {
        // Dashboard Bookmark Elements
        this.addBookmarkIcon = protractor_1.element(protractor_1.by.id("reviewCreateBookmark"));
        this.enterBookmarkButton = protractor_1.element(protractor_1.by.id("reviewbookmarkcommententer"));
        this.bookmarkName = protractor_1.element(protractor_1.by.id("reviewbookmarknamelist"));
        //Grid Locator
        this.gridIcon = protractor_1.element(protractor_1.by.id("grid_icon"));
        this.itemListWindow = protractor_1.element(protractor_1.by.id("item-list"));
        this.gridFirstRecord = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[1]'));
        this.gridSecondRecord = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[2]'));
        this.gridThirdRecord = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[3]'));
        this.gridRecords = protractor_1.element.all(protractor_1.by.xpath('.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"]'));
        this.gridFourth = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[4]'));
        this.gridSixth = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[6]'));
        this.gridSeventh = protractor_1.element(protractor_1.by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[7]'));
        this.griditem = protractor_1.element(protractor_1.by.xpath("//div[@id='itemlistwrapper']//tr[3]//td[2]"));
        this.filterItemCount = protractor_1.element(protractor_1.by.id("search-count"));
        this.bookmarkWidget = protractor_1.element(protractor_1.by.xpath("//div[@id='bookmark_icon']//div[@class='deskclickhandeler']"));
        // menu items
        // closeIcon = element(by.xpath("//div[317]//a[3]//span[1]"));
        this.closeGridIcon = protractor_1.element(protractor_1.by.xpath('.//span[@id="grid_shell_wnd_title"]/parent::div//div[@class="k-window-actions"]//a[@aria-label="Close"]'));
        this.bookmarkOptionMenu = protractor_1.element(protractor_1.by.id("itemlistbookmarkoptionstit"));
        this.bookmarkOptionSelectedMenu = protractor_1.element(protractor_1.by.id("itemlistrcbookmarkselected"));
        this.bookmarkOptionFilterMenu = protractor_1.element(protractor_1.by.id("itemlistrcbookmarkfilterrc"));
        this.bookmarkOptionFilterMenu2 = protractor_1.element(protractor_1.by.xpath("//span[contains(@id,'itemlistrcbookmarkfilterrc')]"));
        this.bookmarkIcon = protractor_1.element(protractor_1.by.xpath("//div[@id='bookmark_icon']//div[@class='deskclickhandeler']"));
        //bookmark assign window
        this.bookmarkAssignWindow = protractor_1.element(protractor_1.by.id("itemlistbookmarkcommentwrapper"));
        this.bookmarkFilterWindow = protractor_1.element(protractor_1.by.id("itemlistbookmarkfilterwrapper"));
        this.selectedBookmarkName = protractor_1.element(protractor_1.by.id("itemlistbookmarkselectactivenode"));
        this.bookmarkEnterButton = protractor_1.element(protractor_1.by.xpath("//button[@id='itemlistbookmarkcommententer']"));
        this.createBookmarkoption = protractor_1.element(protractor_1.by.xpath("//li[@class='context-menu-item context-menu-visible']//span[contains(text(),'Create')]"));
        this.bookmarkNameInput = protractor_1.element(protractor_1.by.xpath("//input[@id='bookmarknameinput']"));
        //filter bookmark window
        this.bookmarkFilterWindowCloseButton = protractor_1.element(protractor_1.by.id("itemlistbookmarkfiltercancel"));
        // Case Locator
        this.selectCaseMainPage = protractor_1.element(protractor_1.by.xpath('.//*[@id="reviewcaseselectorwrapper"]//div[@class="pq-select-text"]'));
        this.selectBookmarkElement = protractor_1.element(protractor_1.by.xpath("//span[@class='fancytree-title'][contains(text(),'Shared')]"));
        this.caseSelectApply = protractor_1.element(protractor_1.by.id("reviewcaseselectorapply"));
        this.selectedCaseLabel = protractor_1.element(protractor_1.by.id("reviewnavBarLoggedInCase"));
        this.selectBookmarkList = protractor_1.element(protractor_1.by.xpath("//span[@class='fancytree-title'][contains(text(),'he')]"));
        this.grid4 = protractor_1.element(protractor_1.by.xpath("//div[@class='k-widget k-window']//tr[5]//td[7]"));
        this.grid2 = protractor_1.element(protractor_1.by.xpath("//div[@class='k-widget k-window']//tr[3]//td[7]"));
        this.grid3 = protractor_1.element(protractor_1.by.xpath("//div[@class='k-widget k-window']//tr[4]//td[7]"));
    }
    /*
//TODO 1.Create bookmark from widget
      2.Open grid and select 3 records
      2A. assigned to created bookmark
      3. Again right click and apply bookmark filter
      4 validate the count


      */
    //1.Create bookmark
    bookmarkPage.prototype.createBookmark = function (bookInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var bookmarkName;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bookmarkName = bookData[bookInfo]['bookName'];
                        //click on + icon to add bookmark
                        return [4 /*yield*/, this.addBookmarkIcon.click()];
                    case 1:
                        //click on + icon to add bookmark
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(2000)];
                    case 2:
                        _a.sent();
                        //enter name and click enter button
                        return [4 /*yield*/, this.bookmarkName.sendKeys(bookmarkName)];
                    case 3:
                        //enter name and click enter button
                        _a.sent();
                        return [4 /*yield*/, this.enterBookmarkButton.click()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(2000)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Select records From Grid
    bookmarkPage.prototype.selectGridRecords = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.gridFourth.click()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(1000)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.gridSixth.click()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //perform Right Click on Selected Grid records
    bookmarkPage.prototype.performRightClickAndAssign = function () {
        return __awaiter(this, void 0, void 0, function () {
            var until;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 1:
                        _a.sent();
                        //perform right click
                        return [4 /*yield*/, protractor_1.browser.actions().click(this.gridThirdRecord, protractor_1.protractor.Button.RIGHT).perform()];
                    case 2:
                        //perform right click
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 3:
                        _a.sent();
                        until = protractor_1.protractor.ExpectedConditions;
                        return [4 /*yield*/, protractor_1.browser.wait(until.presenceOf(this.bookmarkOptionMenu), 7000, 'Element taking too long to appear in the DOM')];
                    case 4:
                        _a.sent();
                        //hover mouse over bookmark option
                        return [4 /*yield*/, protractor_1.browser.actions().mouseMove(this.bookmarkOptionMenu).perform()];
                    case 5:
                        //hover mouse over bookmark option
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, this.bookmarkOptionSelectedMenu.click()];
                    case 7:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(3000)];
                    case 8:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //assign selected records to bookmark from bookmark window
    bookmarkPage.prototype.assignBookmarks = function (bookInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var bookmarkName, bookmarkToSelect;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bookmarkName = bookData[bookInfo]['bookName'];
                        bookmarkToSelect = protractor_1.element(protractor_1.by.xpath('.//div[@id="bookmarktree"]//span[contains(text(), "' + bookmarkName + '")]'));
                        return [4 /*yield*/, bookmarkToSelect.click()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(8000)];
                    case 2:
                        _a.sent();
                        expect(this.selectedBookmarkName.getText()).to.eventually.contain(bookmarkName);
                        return [4 /*yield*/, protractor_1.browser.sleep(2000)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.bookmarkEnterButton.click()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(2000)];
                    case 5:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //perform right click and select options from menu and click "Filter"
    bookmarkPage.prototype.performRightClickAndFilter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var until;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //perform right click
                        protractor_1.browser.actions().click(this.gridFirstRecord, protractor_1.protractor.Button.RIGHT).perform();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 1:
                        _a.sent();
                        until = protractor_1.protractor.ExpectedConditions;
                        protractor_1.browser.wait(until.presenceOf(this.bookmarkOptionMenu), 10000, 'Element taking too long to appear in the DOM');
                        //hover mouse over bookmark option
                        protractor_1.browser.actions().mouseMove(this.bookmarkOptionMenu).perform();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 2:
                        _a.sent();
                        protractor_1.browser.wait(until.presenceOf(this.bookmarkOptionFilterMenu), 10000, 'Element taking too long to appear in the DOM');
                        protractor_1.browser.actions().mouseMove(this.bookmarkOptionFilterMenu).perform();
                        return [4 /*yield*/, this.bookmarkOptionFilterMenu.click()];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 4:
                        _a.sent();
                        expect(this.bookmarkFilterWindow.isDisplayed()).to.eventually.equal(true);
                        return [2 /*return*/];
                }
            });
        });
    };
    //verify assigned bookmarks
    bookmarkPage.prototype.applyFilter = function (bookInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var until, bookmarkName, bookmarkToSelect;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        until = protractor_1.protractor.ExpectedConditions;
                        bookmarkName = bookData[bookInfo]['bookName'];
                        bookmarkToSelect = protractor_1.element(protractor_1.by.xpath('.//div[@id="itemlistbookmarkfilter"]//span[contains(text(), "' + bookmarkName + '")]'));
                        protractor_1.browser.wait(until.presenceOf(bookmarkToSelect), 10000, 'Element taking too long to appear in the DOM');
                        //double click on bookmark name to apply filter
                        protractor_1.browser.actions().mouseMove(bookmarkToSelect).perform();
                        return [4 /*yield*/, protractor_1.browser.actions().doubleClick((bookmarkToSelect)).perform()];
                    case 1:
                        _a.sent();
                        protractor_1.browser.actions().doubleClick((bookmarkToSelect)).perform();
                        return [4 /*yield*/, protractor_1.browser.sleep(5000)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.bookmarkFilterWindowCloseButton.click()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //verify bookmark filter count
    bookmarkPage.prototype.verifyBookmarkFilterCount = function (bookmarkCount) {
        return __awaiter(this, void 0, void 0, function () {
            var bookCount;
            return __generator(this, function (_a) {
                bookCount = bookData[bookmarkCount]['bookCount'];
                expect(this.filterItemCount.getText()).to.eventually.contain(bookCount);
                return [2 /*return*/];
            });
        });
    };
    bookmarkPage.prototype.selectCase = function (caseInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var caseName, caseList, until;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.selectCaseMainPage.click()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, protractor_1.browser.sleep(3000)];
                    case 2:
                        _a.sent();
                        caseName = caseData[caseInfo]["casename"];
                        return [4 /*yield*/, protractor_1.browser.driver.sleep(1000)];
                    case 3:
                        _a.sent();
                        caseList = protractor_1.element.all(protractor_1.by.xpath('.//div[@class="pq-select-menu"]//span[contains(text(), "' + caseName + '")]/parent::label//input'));
                        return [4 /*yield*/, caseList.filter(function (ele) { return ele.isDisplayed(); }).then(function (caseList) { caseList[0].click(); })];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.caseSelectApply.click()];
                    case 5:
                        _a.sent();
                        until = protractor_1.protractor.ExpectedConditions;
                        return [4 /*yield*/, protractor_1.browser.wait(until.presenceOf(this.selectedCaseLabel), 20000, "Element taking too long to appear in the DOM")];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, expect(this.selectedCaseLabel.getText()).to.eventually.contain(caseName)];
                    case 7:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //Open Grid record From dashboard
    bookmarkPage.prototype.clickOnGridOption = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.gridIcon.click()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return bookmarkPage;
}());
exports.bookmarkPage = bookmarkPage;
//# sourceMappingURL=bookmarkPage.js.map