import {browser, element, by, protractor} from "protractor";
import {el} from "@angular/platform-browser/testing/browser_util";

const chai = require("chai").use(require("chai-as-promised"));
const yaml = require("js-yaml");
const fs = require("fs");
let caseData = yaml.safeLoad(fs.readFileSync("../testData/case.yml", "utf8"));
let bookData = yaml.safeLoad(fs.readFileSync('../testData/book.yml', 'utf8'));


const expect = chai.expect;

export class bookmarkPage {
    // Dashboard Bookmark Elements
    addBookmarkIcon = element(by.id("reviewCreateBookmark"));
    enterBookmarkButton = element(by.id("reviewbookmarkcommententer"));
    bookmarkName = element(by.id("reviewbookmarknamelist"));
    //Grid Locator
    gridIcon = element(by.id("grid_icon"));
    itemListWindow = element(by.id("item-list"));


    gridFirstRecord= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[1]'));
    gridSecondRecord= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[2]'));
    gridThirdRecord= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[3]'));
    gridRecords= element.all(by.xpath('.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"]'));
    gridFourth= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[4]'));
    gridSixth= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[6]'));
    gridSeventh= element(by.xpath('(.//div[@id="item-list"]//table[@role="grid"]/tbody/tr//input[@type="checkbox"])[7]'));
    griditem = element(by.xpath("//div[@id='itemlistwrapper']//tr[3]//td[2]"));

    filterItemCount = element(by.id("search-count"));
    bookmarkWidget = element(by.xpath("//div[@id='bookmark_icon']//div[@class='deskclickhandeler']"));

    // menu items
    // closeIcon = element(by.xpath("//div[317]//a[3]//span[1]"));
    closeGridIcon= element(by.xpath('.//span[@id="grid_shell_wnd_title"]/parent::div//div[@class="k-window-actions"]//a[@aria-label="Close"]'));
    bookmarkOptionMenu = element(by.id("itemlistbookmarkoptionstit"));
    bookmarkOptionSelectedMenu = element(by.id("itemlistrcbookmarkselected"));
    bookmarkOptionFilterMenu = element(by.id("itemlistrcbookmarkfilterrc"));
    bookmarkOptionFilterMenu2 = element(by.xpath("//span[contains(@id,'itemlistrcbookmarkfilterrc')]"));
    bookmarkIcon = element(
        by.xpath("//div[@id='bookmark_icon']//div[@class='deskclickhandeler']")
    );

    //bookmark assign window
    bookmarkAssignWindow = element(by.id("itemlistbookmarkcommentwrapper"));
    bookmarkFilterWindow = element(by.id("itemlistbookmarkfilterwrapper"));
    selectedBookmarkName = element(by.id("itemlistbookmarkselectactivenode"));

    bookmarkEnterButton = element(by.xpath("//button[@id='itemlistbookmarkcommententer']"));
    createBookmarkoption=element(by.xpath("//li[@class='context-menu-item context-menu-visible']//span[contains(text(),'Create')]"));
    bookmarkNameInput = element(by.xpath("//input[@id='bookmarknameinput']"));
    //filter bookmark window
    bookmarkFilterWindowCloseButton = element(
        by.id("itemlistbookmarkfiltercancel")
    );
    // Case Locator
    selectCaseMainPage = element(by.xpath('.//*[@id="reviewcaseselectorwrapper"]//div[@class="pq-select-text"]'));
    selectBookmarkElement = element(by.xpath("//span[@class='fancytree-title'][contains(text(),'Shared')]"));
    caseSelectApply = element(by.id("reviewcaseselectorapply"));
    selectedCaseLabel = element(by.id("reviewnavBarLoggedInCase"));
    selectBookmarkList = element(by.xpath("//span[@class='fancytree-title'][contains(text(),'he')]"));
    grid4 = element(by.xpath("//div[@class='k-widget k-window']//tr[5]//td[7]"));

    grid2 = element(by.xpath("//div[@class='k-widget k-window']//tr[3]//td[7]"));

    grid3 = element(by.xpath("//div[@class='k-widget k-window']//tr[4]//td[7]"));

    /*
//TODO 1.Create bookmark from widget
      2.Open grid and select 3 records
      2A. assigned to created bookmark
      3. Again right click and apply bookmark filter
      4 validate the count


      */

//1.Create bookmark
    async createBookmark(bookInfo: string) {
        let bookmarkName = bookData[bookInfo]['bookName'];

        //click on + icon to add bookmark

        await this.addBookmarkIcon.click();
        await browser.sleep(2000);
        //enter name and click enter button
        await this.bookmarkName.sendKeys(bookmarkName);
        await this.enterBookmarkButton.click();
        await browser.sleep(2000);
        //expect(DashboardPage.notificationDiv.getText()).toContain("Getting Bookmark");
    }
    // Select records From Grid
    async selectGridRecords(){
        await  browser.sleep(5000);

        await this.gridFourth.click();
        await  browser.sleep(1000);
        await this.gridSixth.click();
        await  browser.sleep(5000);

    }
//perform Right Click on Selected Grid records
    async performRightClickAndAssign(){
        await  browser.sleep(5000);
        //perform right click
        await browser.actions().click(this.gridThirdRecord, protractor.Button.RIGHT).perform();
        await  browser.sleep(5000);
        //click on Bookmark option
        let until = protractor.ExpectedConditions;
        await browser.wait(until.presenceOf(this.bookmarkOptionMenu), 7000, 'Element taking too long to appear in the DOM');
        //hover mouse over bookmark option
        await browser.actions().mouseMove(this.bookmarkOptionMenu).perform();
        await browser.sleep(5000);
        await this.bookmarkOptionSelectedMenu.click();
        await browser.sleep(3000);
        // expect(this.bookmarkAssignWindow.isDisplayed()).to.eventually.equal(true);
    }

    //assign selected records to bookmark from bookmark window
    async  assignBookmarks(bookInfo: string){
        let bookmarkName = bookData[bookInfo]['bookName'];

        let bookmarkToSelect = element(by.xpath('.//div[@id="bookmarktree"]//span[contains(text(), "'+ bookmarkName + '")]'));
        await bookmarkToSelect.click();
        await browser.sleep(8000);
        expect(this.selectedBookmarkName.getText()).to.eventually.contain(bookmarkName);
        await browser.sleep(2000);
        await this.bookmarkEnterButton.click();
        await browser.sleep(2000);
    }

    //perform right click and select options from menu and click "Filter"
    async performRightClickAndFilter(){

        //perform right click
        browser.actions().click(this.gridFirstRecord, protractor.Button.RIGHT).perform();
        await browser.sleep(5000);
        //click on Bookmark option
        let until = protractor.ExpectedConditions;
        browser.wait(until.presenceOf(this.bookmarkOptionMenu), 10000, 'Element taking too long to appear in the DOM');
        //hover mouse over bookmark option
        browser.actions().mouseMove(this.bookmarkOptionMenu).perform();
        await browser.sleep(5000);
        browser.wait(until.presenceOf(this.bookmarkOptionFilterMenu), 10000, 'Element taking too long to appear in the DOM');
        browser.actions().mouseMove(this.bookmarkOptionFilterMenu).perform();
        await this.bookmarkOptionFilterMenu.click();
        await browser.sleep(5000);
        expect(this.bookmarkFilterWindow.isDisplayed()).to.eventually.equal(true);
    }

    //verify assigned bookmarks
    async applyFilter(bookInfo){
        let until = protractor.ExpectedConditions;
        let bookmarkName = bookData[bookInfo]['bookName'];
        let bookmarkToSelect = element(by.xpath('.//div[@id="itemlistbookmarkfilter"]//span[contains(text(), "'+bookmarkName+'")]'));
        browser.wait(until.presenceOf(bookmarkToSelect), 10000, 'Element taking too long to appear in the DOM');
        //double click on bookmark name to apply filter
        browser.actions().mouseMove(bookmarkToSelect).perform();
        await browser.actions().doubleClick((bookmarkToSelect)).perform();
        browser.actions().doubleClick((bookmarkToSelect)).perform();
        await browser.sleep(5000);
        await this.bookmarkFilterWindowCloseButton.click();
    }

    //verify bookmark filter count
    async verifyBookmarkFilterCount(bookmarkCount){
        let bookCount = bookData[bookmarkCount]['bookCount'];
        expect(this.filterItemCount.getText()).to.eventually.contain(bookCount);
    }


    async selectCase(caseInfo: string) {
        await this.selectCaseMainPage.click();
        await browser.sleep(3000);
        let caseName = caseData[caseInfo]["casename"];

        await browser.driver.sleep(1000);
        let caseList = element.all(by.xpath
        ('.//div[@class="pq-select-menu"]//span[contains(text(), "' + caseName + '")]/parent::label//input'));
        await caseList.filter(function (ele) {return ele.isDisplayed();}).then(function (caseList) {caseList[0].click();});

        await this.caseSelectApply.click();
        var until = protractor.ExpectedConditions;
        await browser.wait(until.presenceOf(this.selectedCaseLabel), 20000, "Element taking too long to appear in the DOM");
        await expect(this.selectedCaseLabel.getText()).to.eventually.contain(caseName);
    }
    //Open Grid record From dashboard
    async clickOnGridOption() {
        await this.gridIcon.click();
    }

}
    