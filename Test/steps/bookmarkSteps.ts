import {Given, Then, When} from "cucumber";
import { bookmarkPage } from "../Pages/bookmarkPage";
import {browser} from "protractor";

const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;
const bookmark = new bookmarkPage();

When(/^i create "([^"]*)" Context$/, {timeout: 5 * 500000}, async(bookmarkInfo) =>{
    await bookmark.createBookmark(bookmarkInfo);

});
When(/^i Select Records from grid$/, {timeout: 5 * 500000}, async() =>{
    await bookmark.selectGridRecords();
    await bookmark.performRightClickAndAssign();
});
When(/^i Assign "([^"]*)" from grid$/,  {timeout: 11 * 50000000}, async(bookmarkInfo) => {
    await bookmark.assignBookmarks(bookmarkInfo);
});
Then(/^i Select Filter$/, {timeout: 5 * 500000}, async() =>{
    await bookmark.performRightClickAndFilter();
});
When(/^i filter "([^"]*)"$/, {timeout: 11 * 50000000}, async(bookmarkInfo) => {
    await bookmark.applyFilter(bookmarkInfo);
});
Then(/^validate filter "([^"]*)"$/, {timeout: 5 * 500000}, async(bookmarkCount) =>{
    await bookmark.verifyBookmarkFilterCount(bookmarkCount);
});
Then(/^user select case "(.*?)"$/, {timeout: 11 * 50000000}, async(caseInfo) => {
    await bookmark.selectCase(caseInfo);
});
When(/^I go to Grid$/,{timeout: 5 * 500000}, async() => {
    await bookmark.clickOnGridOption();
});